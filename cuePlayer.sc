+ CuePlayer { 

    fastForward {arg interval = 10, timelineOptions;
        var time = 0;
        var fastForwardTimeline = Timeline.new(clock, timelineOptions);
        (cueList.size).do{ arg idx;
            var cueNumber = idx + 1;
            var timeline = timelineRegister[cueNumber.asSymbol];
            if(timeline.notNil){
                fastForwardTimeline.add(time, cueList[idx]);
                timeline.functionList.pairsDo{ arg i, function;
                    fastForwardTimeline.add(time, function);
                    time = time + interval;
                };
            }{
                fastForwardTimeline.add(time, cueList[idx]);
                time = time + interval;
            };
        };
        "Starting fastForward".postln;
        fastForwardTimeline.play;
    }

    // stopFastForward {
    //     fastForwardTimeline.stop;
    //     "Stopped fastForward".postln;
    // }

    // startTimelineFrom {arg cueNumber, startTime = 0;
    //     // TODO: copy a cue, and if it has a timeline, shift all the times as needed and copy the new cue back into the cuelist.
    //     var timeline = timelineRegister[cueNumber.asSymbol];
    //     if(timeline.notNil){
    //         var tmp = Timeline.new(clock, timelineOptions); 

    //     }
    // }
}
